package hu.elte.fullstack.bevasarlolista;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetMappingTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate template;

    @Test
    public void getBoltok() throws Exception {
        ResponseEntity<String> response = template.getForEntity("http://localhost:" + port + "/boltok", String.class);
        Integer status = response.getStatusCodeValue();
        assert status.equals(200);
    }

    @Test
    public void getAruk() throws Exception {
        ResponseEntity<String> response = template.getForEntity("http://localhost:" + port + "/aruk", String.class);
        Integer status = response.getStatusCodeValue();
        assert status.equals(200);
    }

    @Test
    public void getBevasarloListak() throws Exception {
        ResponseEntity<String> response = template.getForEntity("http://localhost:" + port + "/bevasarlolistak", String.class);
        Integer status = response.getStatusCodeValue();
        assert status.equals(200);
    }

    @Test
    public void getFelhasznalok() throws Exception {
        ResponseEntity<String> response = template.getForEntity("http://localhost:" + port + "/felhasznalok", String.class);
        Integer status = response.getStatusCodeValue();
        assert status.equals(200);
    }

    @Test
    public void getFutarszolgalatok() throws Exception {
        ResponseEntity<String> response = template.getForEntity("http://localhost:" + port + "/futarszolgalatok", String.class);
        Integer status = response.getStatusCodeValue();
        assert status.equals(200);
    }

    @Test
    public void getHamisVegpont() throws Exception {
        ResponseEntity<String> response = template.getForEntity("http://localhost:" + port + "/hamisvegpont", String.class);
        Integer status = response.getStatusCodeValue();
        assert status.equals(404);
    }
}
