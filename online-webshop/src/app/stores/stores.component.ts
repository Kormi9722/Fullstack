import { Component, OnInit } from '@angular/core';
import { Store} from '../core/store';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit {

  constructor() { }

  stores: Store[]=[
    {
      name: 'Tesco',
      location: 'faraway st. 41.',
      items:[
      {
        name:'tea',
        quantity: 1,
      },
      {
        name:'tészta',
        quantity: 2,
      },
      {
        name:'tojás',
        quantity: 3,
      },
    ],
    },
    {
      name: 'CBA',
      location: 'near rd. 1.',
      items:[
      {
        name:'tea',
        quantity: 10,
      },
      {
        name:'tészta',
        quantity: 22,
      },
      {
        name:'tojás',
        quantity: 33,
      },
      {
        name:'kifli',
        quantity: 6,
      },
      {
        name:'kenyér',
        quantity: 1,
      },
    ],
    },
  
  ];


  ngOnInit(): void {
  }

}
