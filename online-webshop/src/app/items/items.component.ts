import { Component, OnInit } from '@angular/core';
export interface PeriodicElement {
  name: string;
  position: number;
 store: string;
  quantity: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Kenyér', store: 'Tesco', quantity: 5},
  {position: 2, name: 'Kifli', store: 'Tesco', quantity: 20},
  {position: 3, name: 'Tojás', store: 'Tesco', quantity: 12},
  {position: 4, name: 'Tej', store: 'Tesco', quantity: 34},
  {position: 5, name: 'Tea', store: 'Tesco', quantity: 54},
  {position: 6, name: 'Kenyér', store: 'CBA', quantity: 65},
  {position: 7, name: 'Kifli', store: 'CBA', quantity: 32},
  {position: 8, name: 'Csokoládé', store: 'CBA', quantity: 1},
  {position: 9, name: 'Tojás', store: 'CBA', quantity: 12},
  {position: 10, name: 'Kenyér', store: 'Aldi', quantity: 3},
];



@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'store', 'quantity'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit(): void {
  }

}
