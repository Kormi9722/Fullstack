import { Item} from '../core/item';

export interface Store{
    name: string;
    location: string;
    items:Item[];
}