import { Item} from '../core/item';

export interface List{
    name: string;
    description: string;
    items:Item[];
}