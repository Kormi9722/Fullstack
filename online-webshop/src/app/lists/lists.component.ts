import { Component, OnInit } from '@angular/core';
import { List} from '../core/list';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {

  

lists: List[]=[
  {
    name:'Lista 1',
    description:'Ez egy gyümölcs lista',
    items:[
      {
        name:'alma',
        quantity: 5,
      },
    ],  
  },

  {
    name:'Lista 2',
    description:'Ez egy másik lista',
    items:[
      {
        name:'tea',
        quantity: 1,
      },
      {
        name:'tészta',
        quantity: 2,
      },
      {
        name:'tojás',
        quantity: 3,
      },
    ],  
  },

  {
    name:'Lista 3',
    description:'Ez egy harmadik lista',
    items:[
      {
        name:'kifli',
        quantity: 6,
      },
      {
        name:'kenyér',
        quantity: 1,
      },
    ],  
  },

];

  constructor() { }

  ngOnInit(): void {
    
  }

}
