import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemsComponent } from './items/items.component';
import { ListsComponent } from './lists/lists.component';
import { StoresComponent } from './stores/stores.component';
import { UserComponent } from './user/user.component';
import { UserItemListComponent } from './user-item-list/user-item-list.component';

const routes: Routes = [
{
  path: 'lists',
  component: ListsComponent,
},
{
  path: 'stores',
  component: StoresComponent,
},
{
  path: 'items',
  component: ItemsComponent,
},

{
  path: 'user',
  component: UserItemListComponent,
},

{
  path: '**',
  redirectTo: 'lists',
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
