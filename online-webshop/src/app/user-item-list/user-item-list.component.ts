import { Component, OnInit } from '@angular/core';

@Component({
selector: 'app-user-item-list',
templateUrl: './user-item-list.component.html',
styleUrls: ['./user-item-list.component.scss']
})
export class UserItemListComponent implements OnInit {

userList =[ { name: 'User 1' }, { name: 'User 2' }];

constructor() { }

  ngOnInit(): void {
  }

}
